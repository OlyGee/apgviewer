var turf = require('@turf/turf');
console.log("welcome to turfinator...")
console.log("loading point and polgon...")

var pt = turf.point([-77, 44]);
var poly = turf.polygon([[
  [-81, 41],
  [-81, 47],
  [-72, 47],
  [-72, 41],
  [-81, 41]
]]);

console.log('checking to see if point is in polygon...')
console.log(turf.booleanPointInPolygon(pt, poly))
